const fs = require('fs');

function part1() {
  const allFilePath = [];

  for (let id = 0; id < 3; id++) {
    let filePath = `file${id}.json`;
    allFilePath.push(filePath);

    fs.writeFile(filePath, 'This is Json File', (err) => {
      if (err) {
        console.log('error occured: ', err);
        return;
      } else {
        console.log('all Json are Created');
      }
    });
  }

  allFilePath.map((filePath) => {
    fs.unlink(filePath, (err) => {
      if (err) {
        console.log('error occured: ', err);
        return;
      } else {
        console.log('All Json are deleted');
      }
    });
  });
}

function part2() {
  function creatingAllFile() {
    return new Promise((resolve, reject) => {
      const allFilePath = [];

      for (let id = 0; id < 3; id++) {
        let filePath = `file${id}.json`;
        allFilePath.push(filePath);

        fs.writeFile(filePath, 'This is Json File', (err) => {
          if (err) {
            reject('Error occured: ', err);
          } else {
            console.log(filePath);
            resolve(allFilePath);
          }
        });
      }
    });
  }

  creatingAllFile()
    .then((allFilePath) => {
      const deletingJsonFiles = new Promise((resolve, reject) => {
        allFilePath.map((filePath) => {
          fs.unlink(filePath, (err) => {
            if (err) {
              reject('Error occured: ', err);
            } else {
              console.log('deleting:', filePath);

              resolve('All Json Files are deleted.');
            }
          });
        });
      });

      deletingJsonFiles
        .then((responseOfDeletingFile) => {
          console.log(responseOfDeletingFile);
        })
        .catch((err) => {
          console.log(err);
        });
    })
    .catch((err) => {
      console.log(err);
    });
}
function creatingFiles() {
  return new Promise((resolve, reject) => {
    const allFilePath = [];

    for (let id = 0; id < 3; id++) {
      let filePath = `file${id}.json`;
      allFilePath.push(filePath);

      fs.writeFile(filePath, 'This is Json File', (err) => {
        if (err) {
          reject('Error occured: ', err);
        } else {
          resolve(allFilePath);
        }
      });
    }
  });
}

function deletingFile(allFilePath) {
  return new Promise((resolve, reject) => {
    allFilePath.map((filePath) => {
      fs.unlink(filePath, (err) => {
        if (err) {
          reject('Error occured: ', err);
        } else {
          resolve('All Json Files are deleted.');
        }
      });
    });
  });
}

async function part3() {
  try {
    const allFilePath = await creatingFiles();
    console.log('all Json are created');
    const allFileDeleting = await deletingFile(allFilePath);
    console.log(allFileDeleting);
  } catch (err) {
    console.log(err);
    return;
  }
}

module.exports = { part1, part2, part3 };
