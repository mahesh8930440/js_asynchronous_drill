# Asynchronous drills Project

## Data Files

- The data for this project can be downloaded from Lipsum Website.

## Project Setup

- Before starting the implementation, make sure to follow these setup instructions:
  - Create a new repository with the name "js_asynchronous_drill" in your Gitlab subgroup.

## Getting Started

## Steps to setup and run the project

### 1. Clone the repo from GitHub\*\*

- git clone https://gitlab.com/mahesh8930440/js_asynchronous_drill.git

### 2. Install node and npm.

- sudo apt install nodejs;sudo apt install npm

### 3. Navigate to the project directory `js_asynchronous_drill`.

- cd js_asynchronous_drill

### 4. Install npm packages

- This command will install all the dependencies required

  - npm install

### 5. Run the command to see the output

- node TEST/testProblem1.js

- node TEST/testProblem2.js
