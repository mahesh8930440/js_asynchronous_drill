const fs = require('fs/promises');

async function appendingUpperCaseDataIntoFiles(transformedUpperCaseData) {
  await fs.writeFile('uppercase.txt', transformedUpperCaseData);
  await fs.writeFile('filenames.txt', 'uppercase.txt' + ',');
  console.log('Data is appended into uppercase and filename.txt files');
}

function convertingDataIntoLowerCase(data) {
  let transformedLowerCaseData = data.toLowerCase();
  console.log('Data converted into lowercase');
  return transformedLowerCaseData;
}

async function creatingFileForEachSentences(transformedLowerCaseData) {
  const allSentences = transformedLowerCaseData.split(/\./);
  let fileCount = 0;

  for (let sentence of allSentences) {
    let newFilePath = 'file' + fileCount + '.txt';
    fileCount += 1;

    await fs.writeFile(newFilePath, sentence);

    await fs.appendFile('filenames.txt', newFilePath + ',');
    console.log(newFilePath + ' is created');
  }
  console.log('Files are created for each sentences');
}

async function sortingWholeContents(filePath) {
  const fileNameString = await fs.readFile(filePath, 'utf-8');
  const allFileNames = fileNameString.match(/[A-Za-z0-9.]+/g);

  for (let filename of allFileNames) {
    const sentence = await fs.readFile(filename, 'utf-8');

    if (filename !== 'uppercase.txt') {
      let allWords = sentence.split(' ');
      allWords.sort();
      let sortedSentence = allWords.join(' ');

      await fs.appendFile('sorted.txt', sortedSentence + '.');
    }
  }

  await fs.appendFile('filenames.txt', 'sorted.txt');
  console.log('Each sentence is sorted');
}

async function deletingAllFiles(filePath) {
  const fileNameString = await fs.readFile(filePath, 'utf-8');
  const allFilePaths = fileNameString.match(/[A-Za-z0-9.]+/g);

  for (let filePath of allFilePaths) {
    await fs.unlink(filePath, (err) => {
      if (err) {
        console.log(err);
      }
    });
    console.log(filePath + ' is deleted');
  }
  console.log('all Files are deleted');
}

async function problem2() {
  try {
    const data = await fs.readFile('lipsumData.txt', 'utf-8');
    let transformedUpperCaseData = data.toUpperCase();
    console.log('Data converted into uppercase');
    await appendingUpperCaseDataIntoFiles(transformedUpperCaseData);

    const transformedLowerCaseData = convertingDataIntoLowerCase(
      transformedUpperCaseData,
    );

    await creatingFileForEachSentences(transformedLowerCaseData);
    await sortingWholeContents('filenames.txt');
    await deletingAllFiles('filenames.txt');
  } catch (err) {
    console.log(err);
  }
}
problem2();
module.exports = problem2;
